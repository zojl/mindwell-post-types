const form = $('#entry-editor');
const holder = form.parent();
let formContents = form.find('input[name="title"], textarea[name="content"]');
let formTitle = form.find('input[name="title"]');
let formText = form.find('textarea[name="content"]');
let formControls = form.find('fieldset.form-group.label-floating.is-select, .switcher-block.for-public-only, #post-entry');
let preForm = $('<div>').addClass('pre-form').prependTo(holder);
const getBack = $('<div>').text('« другой тип записи').addClass('get-back');
preForm.on('click', '.get-back', ()=>{showMenu()});

function showMenu() {
	clearForms();
	let menuPost = $('<div>').addClass('menu-point').addClass('menu_post').text('Пост');
	let menuImage = $('<div>').addClass('menu-point').addClass('menu_image').text('Картинка').click(showImageForm);
	let menuLink = $('<div>').addClass('menu-point').addClass('menu_link').text('Ссылка').click(showLinkForm);
	let menuQuote = $('<div>').addClass('menu-point').addClass('menu_quote').text('Цитата').click(showQuoteForm);
	let menuHolder = $('<div>').addClass('menu-holder');
	menuHolder
		.append(menuPost)
		.append(menuImage)
		.append(menuLink)
		.append(menuQuote);
	formContents.hide();
	let preMenu = $('<div>').text('Что запостим?').addClass('pre-menu_question').append(menuHolder);
	preForm.html(preMenu);
}

function showImageForm() {
	clearForms();
	preForm.html('').append(getBack);
	let imageForm = $('<div>');
	let imageURLInputGroup = $('<div>').addClass('form-group').appendTo(imageForm);
	let imageAltInputGroup = $('<div>').addClass('form-group').appendTo(imageForm);
	let imageDescrInputGroup = $('<div>').addClass('form-group').appendTo(imageForm);
	let imageURLInput = $('<input>').addClass('pre-form_image-url').addClass('form-control').prop('placeholder', 'URL картинки').prop('maxlength', 800).appendTo(imageURLInputGroup);
	let imageAltInput = $('<input>').addClass('pre-form_image-alt').addClass('form-control').prop('placeholder', 'Альтернативный текст').prop('maxlength', 400).appendTo(imageAltInputGroup);
	let imageDescrInput = $('<textarea>').addClass('pre-form_image-descr').addClass('form-control').prop('placeholder', 'Текст под картинкой').prop('maxlength', 5000).appendTo(imageDescrInputGroup);
	imageForm.prependTo(preForm)
		.find(':input')
			.on('focus', (e) => {
				$(e.delegateTarget).parent().addClass('is-focused');
			})
			.on('focusout', (e) => {
				$(e.delegateTarget).parent().removeClass('is-focused');
			});
	formContents.hide();

	const isValidUrl = (string) => {
		try {
			new URL(string);
			return true;
		} catch (_) {
			return false;
		}
	}

	const render = () => {
		let result = '';
		let imageURL = imageURLInput.val();
		let imageAlt = imageAltInput.val();
		let imageDescr = imageDescrInput.val();
		if (!imageAlt) {
			imageAlt = 'Картинка без описания: ' + imageURL;
		}
		if (imageURL && isValidUrl(imageURL)) {
			result = `[![${imageAlt}](${imageURL})](${imageURL})`;
			if (imageDescr) {
				result += `\n___\n${imageDescr}`;
			}
		}
		return result ? result : false;
	}

	imageForm.find(':input').on('change, keyup', () => {
		let post = render();
		if (!post) {
			imageURLInputGroup.addClass('has-error');
			formText.val('');
		} else {
			imageURLInputGroup.removeClass('has-error');
			formText.val(post);
		}
	});
}

function showLinkForm() {
	clearForms();
	preForm.html('').append(getBack);
	let linkForm = $('<div>');
	let linkURLInputGroup = $('<div>').addClass('form-group').appendTo(linkForm);
	let linkTextInputGroup = $('<div>').addClass('form-group').appendTo(linkForm);
	let linkDescrInputGroup = $('<div>').addClass('form-group').appendTo(linkForm);
	let linkURLInput = $('<input>').addClass('pre-form_link-url').addClass('form-control').prop('placeholder', 'URL ссылки').prop('maxlength', 300).appendTo(linkURLInputGroup);
	let linkTextInput = $('<input>').addClass('pre-form_link-text').addClass('form-control').prop('placeholder', 'Текст ссылки').prop('maxlength', 32).appendTo(linkTextInputGroup);
	let linkDescrInput = $('<textarea>').addClass('pre-form_link-descr').addClass('form-control').prop('placeholder', 'Описание ссылки').prop('maxlength', 1000).appendTo(linkDescrInputGroup);
	linkForm.prependTo(preForm)
		.find(':input')
			.on('focus', (e) => {
				$(e.delegateTarget).parent().addClass('is-focused');
			})
			.on('focusout', (e) => {
				$(e.delegateTarget).parent().removeClass('is-focused');
			});
	formContents.hide();

	const isValidUrl = (string) => {
		try {
			new URL(string);
			return true;
		} catch (_) {
			return false;
		}
	}

	const render = () => {
		let result = '';
		let linkURL = linkURLInput.val();
		let linkText = linkTextInput.val();
		let linkDescr = linkDescrInput.val();
		if (!linkText) {
			linkText = linkURL.length <= 32 ? linkURL : linkURL.substr(0, 29) + '...';
		}
		if (linkURL && isValidUrl(linkURL)) {
			result = `# [${linkText}](${linkURL})`;
			if (linkDescr) {
				result += `\n___\n${linkDescr}`;
			}
		}
		return result ? result : false;
	}

	linkForm.find(':input').on('change, keyup', () => {
		let post = render();
		if (!post) {
			linkURLInputGroup.addClass('has-error');
			formText.val('');
		} else {
			linkURLInputGroup.removeClass('has-error');
			formText.val(post);
		}
	});
}

function showQuoteForm() {
	clearForms();
	formContents.hide();
	preForm.html('').append(getBack);
	let quoteForm = $('<div>');
	quoteForm.prependTo(preForm)
		.find(':input')
			.on('focus', (e) => {
				$(e.delegateTarget).parent().addClass('is-focused');
			})
			.on('focusout', (e) => {
				$(e.delegateTarget).parent().removeClass('is-focused');
			});
	let quoteTextInputGroup = $('<div>').addClass('form-group').appendTo(quoteForm);
	let quoteAuthorInputGroup = $('<div>').addClass('form-group').appendTo(quoteForm);
	let quoteTextInput = $('<input>').addClass('pre-form_quote-text').addClass('form-control').prop('placeholder', 'Текст цитаты').prop('maxlength', 300).appendTo(quoteTextInputGroup);
	let quoteAuthorInput = $('<input>').addClass('pre-form_quote-author').addClass('form-control').prop('placeholder', 'Автор цитаты').prop('maxlength', 300).appendTo(quoteAuthorInputGroup);

	const render = () => {
		let result = '';
		let quoteText = quoteTextInput.val();
		let quoteAuthor = quoteAuthorInput.val();
		if (quoteText) {
			result = `> ## «${quoteText}»`;
			if (quoteAuthor) {
				result += `\n*—${quoteAuthor}*`;
			}
		}
		return result ? result : false;
	}

	quoteForm.find(':input').on('change, keyup', () => {
		let post = render();
		if (!post) {
			quoteTextInputGroup.addClass('has-error');
			formText.val('');
		} else {
			quoteTextInputGroup.removeClass('has-error');
			formText.val(post);
		}
	});
}

function clearForms() {
	preForm.html('');
	formText.val('');
	formTitle.val('');
}

showMenu();